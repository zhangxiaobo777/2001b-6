const Koa = require('koa');
const app = new Koa();

const router = require('koa-router')();

//引入sdk

const AlipaySDK = require('alipay-sdk').default;
const AlipayFormData = require('alipay-sdk/lib/form').default;

// 创建AlipaySDK
const alipaySdk = new AlipaySDK({
    appId: '2021000121670807', // 开放平台上创建应用时生成的 appId
    signType: 'RSA2', // 签名算法,默认 RSA2
    gateway: 'https://openapi.alipaydev.com/gateway.do', // 支付宝网关地址 ，沙箱环境下使用时需要修改
    alipayPublicKey: `MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhPlMFFRpZ4zEctpNKBncPuIxkST1orQM9tdAKScr3lvmBlVC525oBw6WNOpOyQ13YW6C1QXRfLjyfsQ+lJuBwv9xDn12v6SgpBVVg536szPCWTiRWfoJl6Cs/64AOMRlPk/HSmSivs0PvqGnDhvAjv2LqGo07AOWo24AwHb3Y5RnfzGKaxZnsN3BUfCPChWzWm60SG07WYSai+n3Gnn6QSqtGOwvM7a/SLYSDHnY/qRUuGkootozMk5PZlPZ2GUt73YVjX4l7lPw8FOvfnvOtTsMjVBo1u33Dl/eUdI6DCho4lSR1/lnhKHFH4GzKx5pqLi+flKdx4+OM/nnQaEvJwIDAQAB`, // 支付宝公钥，需要对结果验签时候必填
    privateKey: 'MIIEowIBAAKCAQEAxmurRtDkFfZQtqwKAtk6e8uSXCN3gutNNgjSA9an/sXh0zRePFcNtbMuAjOLsNgyfwJC2hxww9SA74SARqy0kaU5H3FumHekMK6hYfehiGGI6BmYhdcUPtfHpbpwg5oZclVCEiJe2iD4CSFtVWVZ55sxlxTuwsPNXPVL8L1h7WHhHPCHIg9Qw7CZ6lgOD/lRSONVS603IXGbFC5oURKaO5SKhj0Xi4D1OB4HHLS8r2ZA06a5SSN0j9PqM9pY+Tzo2UUAHU8Ccm/GgarMmxlDvmeztgr65bRvmgSq54z7ciRBJvfQTaEtRP34UVnVab+DucMq/XOqYmDfyXtIjin4NQIDAQABAoIBAQCUfsnNEtJQGWpoe7J2VUpukXi0mhp8EUtUkB9UnhZWFpQeM4f/EouBG1robNkzfWm1IeUURMqj1Gq2VmyF+ASfJ47rmEHTu15WzKqcne33Gj+SrG04ZTGSonyY7EH+S5qVtXbgCXcBOkFG3AxuJ460RbJERnc1rcIIus+dmK4sL4ueDEBDUM5EF33zhxMm8nT5tNwjTlw0eL9aRvg4/6ciBd0NAKF5Gx++rPwd+5BTbQUiRsCMQh/G/0IT42G9ql5AoJO0O5bj5Db76XvqmHG4szH8lQk7W9LK/bEPkGNtE2z4puAttzhQyfgbpgpu1rIKjLThNkbROGMYhlomvgAVAoGBAP7IZZfiYs9JT9JrKqSNNHb/j5GoQ8EFJ7w68VRZNRleHOFvl4VdhbzTMEyRUKLBd6fbIPKvytAnrUZVR2E4fY7UkE/+CNW0CPs90cOXkw8METajBXZQO5KHW9bAOSbQmkXouTdnCEnQ4Vovb8jrX2VvUCz/be9w38kvk+1mtXo3AoGBAMdeVyKBnc9VF2ChkC6eiI6/zMYF0/OqgmCm28NqhbSs5nA9EayqJlQrDRqlUrVW749q5yVqhM3X+8BQmgwefSNAGA9bnBzpGgFv+Ip44efBZtZu2l/nja2WRX3CaApc+K5ixseaA883szNJU2dVjElFoLEUEZQM580EcluaPbrzAoGAZbE6y4qygaXzVDsuBAvQOwb6GNseX/Zcy8aC+HW6uetUf654veR8MsfgaU8+7hdmWPsfhX4g50BO6tmy6d0QUGgLScJNaG65fv5HsVfYfJMGc9ZRoc2YWX3pbCfBJ/ru+KRQ+QoIxt22dLamWX2I0AXD7JHvITjeOYDh/EmYebUCgYAn1TuHk2oaUJ/5GI6cIeSa0MtzDRsJ5hligjQKWTAZpynrZugvHVfHHUri9gEOZi4681r3aM7b6blM9U2x+i16a4XjG6SXSiN+Agcgc4J/MW8AjOlom/T8p7d+pubumyC8htO8cZMtRa2yI4ZccQU0ZoryllXkxKdTmP3YO3r17QKBgBLphtt2Wr1/s1KKXS+5DJ47KrJeUHwUp8kI/XCROZTeRyGBz6b8g/VvB0bou6K8s5VaVp/YRI+qTpo5Zn/j6Vr+ALyeFEWypwYk7fRWqxVtd5KoQL7b8dMbn4N4IsoLjD1JU1w7e00QEY5zndpSey68Kk9knAJtB5P7fneX2M9s', // 应用私钥字符串
});
//支付接口
router.get('/pay', async ctx => {
    const formData = new AlipayFormData();
    formData.setMethod('get');
    formData.addField('notifyUrl', 'https://www.baidu.com');
    formData.addField('bizContent', {
        outTradeNo: '15693802373221aaa', // 商户订单号,64个字符以内、可包含字母、数字、下划线,且不能重复
        productCode: 'FAST_INSTANT_TRADE_PAY', // 销售产品码，与支付宝签约的产品码名称,仅支持FAST_INSTANT_TRADE_PAY
        totalAmount: '1000', // 订单总金额，单位为元，精确到小数点后两位
        subject: '商品', // 订单标题
        body: '商品详情', // 订单描述

    });
    formData.addField('returnUrl', 'http://localhost:3000/home/article');
    const result = await alipaySdk.exec(  // result 为可以跳转到支付链接的 url
        'alipay.trade.page.pay', // 统一收单下单并支付页面接口
        {}, // api 请求的参数（包含“公共请求参数”和“业务参数”）
        { formData: formData },
    );
    ctx.body = {
        code: 1,
        result,
    };
});

app.use(router.routes(), router.allowedMethods());
app.listen(9000, () => {
    console.log(`service is running at 9000`)
});