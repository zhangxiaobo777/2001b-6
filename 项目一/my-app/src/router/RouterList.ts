import { lazy } from 'react';

const routers = [
    {
        path: '/home',
        name: 'Home',
        component: lazy(() => import('../views/home')),
        children: [
            {
                path: '/home/article',
                name: 'article',
                component: lazy(() => import('../views/article')),
            },
            {
                path: '/home/archive',
                name: 'archive',
                component: lazy(() => import('../views/archive')),
            },
            {
                path: '/home/knowlage',
                name: 'knowlage',
                component: lazy(() => import('../views/knowlage')),
            },
            //切换tag筛选
            {
                path: '/home/tag/:id',
                name: 'tag',
                component: lazy(() => import('../views/tag')),
            },
            //知识小册详情
            {
                path: '/home/knowdetail/:id',
                component: lazy(() => import('../components/knowdetail/Index')),
            },
            //轮播图跳详情
            {
                path: '/home/details/:id',
                name: '文章详情',
                component: lazy(() => import('../views/dateils')),
            },
            {
                path: '/home/categoryTitle/:id',
                name: 'categoryTitle',
                component: lazy(() => import('../views/categoryTitle')),
            },
            {
                path: '/home',
                redirect: '/home/article',
            },
        ],
    },
    {
        path: '/search',
        name: 'search',
        component: lazy(() => import('../views/search')),
    },
    {
        path: '/',
        redirect: '/home/article',
    },
];

export default routers;
