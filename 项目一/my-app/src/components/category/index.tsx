import style from '../../style/style.module.css';
import { Mydispatch } from '../../utils/type';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import { get_type_list } from '../../api/api';
import './index.css';
import { useNavigate } from 'react-router-dom';
type Props = {};
const Category = (props: Props) => {
    const dispatch: Mydispatch = useDispatch();
    const navigate = useNavigate();
    useEffect(() => {
        dispatch(get_type_list());
    }, [dispatch]);
    const { typelist } = useSelector(({ store }: any) => {
        return { ...store };
    });

    return (
        <div className={style.recommendToReading_box}>
            <div className="rightList">
                <div className="Titletn">
                    <li className="titleC">categoryTitle</li>
                </div>
                <ul className="ULs-itms">
                    {typelist.map((item: any, index: number) => {
                        return (
                            <li
                                key={index}
                                onClick={() =>
                                    navigate(`/home/categoryTitle/${item.label}`, {
                                        state: {
                                            index,
                                        },
                                    })
                                }
                                style={{
                                    animation: `example1 ${(index + 1) * 0.1}s ease-out ${
                                        (index + 2) * 0.1
                                    }s backwards`,
                                }}
                            >
                                {' '}
                                <span>{item.label}</span> <span>Total{item.articleCount}count</span>
                            </li>
                        );
                    })}
                </ul>
            </div>
        </div>
    );
};
export default Category;
