import { Carousel, Image } from 'antd';
import { useSelector, useDispatch } from 'react-redux';
import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import './Myswiper.css';
import * as api from '../../api/api';
import style from '../../style/style.module.css';
import { time } from '../../utils/time';
const contentStyle: React.CSSProperties = {
    width: '100%',
    height: '316px',
};
type Props = {};
const Myswiper = (props: Props) => {
    const dispatch: any = useDispatch();
    const navigate = useNavigate();
    useEffect(() => {
        dispatch(api.get_Articlet());
    }, []);
    const Article = useSelector((state: any) => state.store.Article);
    let brr = Article.filter((item: any) => {
        return item.cover;
    });

    return (
        <div className={style.Myswiper}>
            <Carousel autoplay>
                {brr.map((item: any, index: number) => {
                    return (
                        <div
                            key={index}
                            className="swiper_content"
                            onClick={() => {
                                navigate(`/home/details/${item.id}`, { state: { index } });
                                dispatch(api.toDateils(item.id));
                            }}
                        >
                            <Image src={item.cover} alt="" width="100%" style={contentStyle} />
                            <div className="swiper_mask">
                                <div>
                                    <h2>{item.title}</h2>
                                    <p>
                                        {/* {time(item.publishAt)} */}
                                        &emsp; {time(item.publishAt)}&emsp;readingCountTemplate
                                    </p>
                                </div>
                            </div>
                        </div>
                    );
                })}
            </Carousel>
        </div>
    );
};

export default Myswiper;
