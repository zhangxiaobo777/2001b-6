/* eslint-disable array-callback-return */
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import * as api from '../../api';
import { date } from '../../utils/time';
import './index.css';
import { Affix, Breadcrumb, Button, Empty } from 'antd';
import Shore from '../../components/shore';
import style from '../../style/style.module.css';
import BackTop from '../backtop';
import { EyeOutlined, ShareAltOutlined } from '@ant-design/icons';

const Index = () => {
    const dispatch: any = useDispatch();
    const navigate = useNavigate();
    const params = useParams();
    useEffect(() => {
        dispatch(api.getknowDetail(params.id as string));
    }, [dispatch]);
    const modalObj = useSelector((state: any) => state.store.knowlistinfomodal);
    const [isModalOpen, setIsModalOpen] = useState(false);
    useEffect(() => {
        dispatch(api.get_knowlist());
    }, [dispatch]);
    const knowlist = useSelector((state: any) => state.store.knowlist);
    const leftarr = knowlist.filter((item: any) => item.id === params.id)[0];
    const rightarr = knowlist.filter((item: any) => item.id !== params.id);
    return (
        <div className="know_con_box">
            <Breadcrumb>
                <Breadcrumb.Item>
                    <p>
                        <a href="/home/knowlage">knowledgeBooks</a> / {leftarr.title}
                    </p>
                </Breadcrumb.Item>
            </Breadcrumb>
            <div className="con_box">
                <div className="con_left_box">
                    <div id="knowdetail">
                        <h3>{leftarr.title}</h3>
                        <div id="div" className={style.tag_box}>
                            <img src={leftarr.cover} alt="" />
                            <h3>{leftarr.summary}</h3>
                            <p>{date(leftarr.createAt)}</p>
                            <div className="btn_icon_box">
                                <Button disabled>startReading</Button>
                            </div>
                            <p>pleaseWait</p>
                        </div>
                    </div>
                </div>
                <Affix>
                    <div className="con_right_box">
                        <h3>otherKnowledges</h3>
                        <div className="recommendToReading_box" id={style.Comments_com}>
                            <ul className="ulright">
                                {rightarr.length ? (
                                    rightarr.map((item: any, index: number) => {
                                        if (item.status === 'publish') {
                                            return (
                                                <li
                                                    key={index}
                                                    onClick={() =>
                                                        navigate(`/home/knowdetail/${item.id}`)
                                                    }
                                                >
                                                    <p>
                                                        <h3>{item.title}</h3>&emsp;
                                                        <span>{date(item.createAt)}</span>
                                                    </p>
                                                    <dl>
                                                        <dt>
                                                            <img src={item.cover} alt="" />
                                                        </dt>
                                                        <dd>
                                                            <h4>{item.summary}</h4>
                                                            <p>
                                                                <span>
                                                                    <EyeOutlined />
                                                                </span>
                                                                <span>{item.views}</span>
                                                                <span
                                                                    onClick={(e) => {
                                                                        e.stopPropagation();
                                                                    }}
                                                                >
                                                                    <ShareAltOutlined
                                                                        onClick={() => {
                                                                            dispatch(
                                                                                api.getknowDetailModal(
                                                                                    item.id
                                                                                )
                                                                            );
                                                                            setIsModalOpen(true);
                                                                        }}
                                                                    />
                                                                    share
                                                                </span>
                                                            </p>
                                                        </dd>
                                                    </dl>
                                                </li>
                                            );
                                        }
                                    })
                                ) : (
                                    <Empty />
                                )}
                            </ul>
                        </div>
                    </div>
                </Affix>
            </div>

            <Shore
                modalObj={modalObj}
                handleCancel={() => {
                    setIsModalOpen(false);
                }}
                openss={isModalOpen}
            ></Shore>
            <BackTop></BackTop>
        </div>
    );
};

export default Index;
