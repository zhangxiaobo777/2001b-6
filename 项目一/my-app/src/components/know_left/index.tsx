import './index.css';
import { useSelector, useDispatch } from 'react-redux';
import { useEffect, useState } from 'react';
import './index.css';
import { EyeOutlined, ShareAltOutlined } from '@ant-design/icons';
import Shore from '../../components/shore';
import * as api from '../../api';
import { useNavigate } from 'react-router-dom';
import style from '../../style/style.module.css';
import { time } from '../../utils/time';
import { Empty } from 'antd';
type Props = {};
const Index = (props: Props) => {
    const navigate = useNavigate();
    const dispatch: any = useDispatch();
    const [isModalOpen, setIsModalOpen] = useState(false);
    const knowlist = useSelector((state: any) => state.store.knowlist);
    const modalObj = useSelector((state: any) => state.store.knowlistinfo);
    useEffect(() => {
        dispatch(api.get_knowlist());
    }, [dispatch]);
    return (
        <div id="knowitem" className={style.tag_box}>
            <ul>
                {knowlist.length ? (
                    knowlist.map((item: any, index: number) => {
                        return (
                            <li
                                key={index}
                                onClick={() => {
                                    navigate(`/home/knowdetail/${item.id}`);
                                }}
                            >
                                <div className="know_nav_title">
                                    <b>{item.title}</b>
                                    <span>{time(item.publishAt)}</span>
                                </div>
                                <div className="know_content_list">
                                    {item.cover ? (
                                        <dl>
                                            <dt>
                                                <img src={item.cover} alt="" />
                                            </dt>
                                            <dd>
                                                <div>
                                                    <span>{item.summary}</span>
                                                </div>
                                                <div className="banban">
                                                    <span>
                                                        <EyeOutlined /> {item.views}
                                                    </span>
                                                    <span
                                                        onClick={(e) => {
                                                            e.stopPropagation();
                                                            dispatch(api.getknowDetail(item.id));
                                                            setIsModalOpen(true);
                                                        }}
                                                    >
                                                        {' '}
                                                        <ShareAltOutlined /> shore
                                                    </span>
                                                </div>
                                            </dd>
                                        </dl>
                                    ) : (
                                        <div className="know_txt_box">
                                            <div>
                                                <span>{item.summary}</span>
                                            </div>
                                            <div className="banban">
                                                <span>
                                                    <EyeOutlined /> {item.views}
                                                </span>
                                                <span
                                                    onClick={(e) => {
                                                        e.stopPropagation();
                                                        dispatch(api.getknowDetail(item.id));
                                                        setIsModalOpen(true);
                                                    }}
                                                >
                                                    {' '}
                                                    <ShareAltOutlined /> shore
                                                </span>
                                            </div>
                                        </div>
                                    )}
                                </div>
                            </li>
                        );
                    })
                ) : (
                    <Empty>暂无数据</Empty>
                )}
            </ul>
            <Shore
                modalObj={modalObj}
                handleCancel={() => {
                    setIsModalOpen(false);
                }}
                openss={isModalOpen}
            ></Shore>
        </div>
    );
};

export default Index;
