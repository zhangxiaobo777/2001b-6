import { useState } from 'react';
import { MessageOutlined } from '@ant-design/icons';
import './comments.css';
import './markdown.less';
import CommentInputBox from '../commentInputBox';
import { time } from '../../utils/time';
import style from '../../style/style.module.css';
type Props = {
    list?: any;
};

const Comments = (props: Props) => {
    let { list } = props;
    const [flag, setflag] = useState(false);
    const [ind, setInd] = useState(-1);
    return (
        <div className="Comments_com" id={style.Comments_com}>
            {list &&
                list.map((item: any, index: number) => {
                    return (
                        <div key={index} className="Comments_com_box" id={style.Comments_com}>
                            <div className="Comments_top">
                                <span
                                    className="portrait"
                                    style={{
                                        background:
                                            '#' + (~~(Math.random() * (1 << 24))).toString(16),
                                    }}
                                >
                                    {item.name.substr(0, 1)}
                                </span>
                                <span className="name">{item.name}</span>
                            </div>
                            <div className="Comments_main">
                                {item.content ? (
                                    <div>{item.content}</div>
                                ) : (
                                    <div
                                        className="markdown"
                                        dangerouslySetInnerHTML={{ __html: item.html }}
                                    ></div>
                                )}
                            </div>
                            <div className="Comments_footer">
                                <span>{item.userAgent}</span>
                                <span>{time(item.createAt)}</span>
                                <span
                                    className="reply"
                                    onClick={() => {
                                        flag ? setflag(false) : setflag(true);
                                        setInd(index);
                                    }}
                                >
                                    <MessageOutlined />
                                    回复
                                </span>
                                {flag && ind === index ? (
                                    <CommentInputBox isRelease={true} Stow={() => setflag(false)} />
                                ) : (
                                    <></>
                                )}
                            </div>
                            {item.children && <Comments list={item.children} />}
                        </div>
                    );
                })}
        </div>
    );
};

export default Comments;
