import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import * as api from '../../api/api';
import './css/index.css';
import style from '../../style/style.module.css';
import { useNavigate } from 'react-router-dom';
import { time } from '../../utils/time';
type Props = {};
const Archives = (props: Props) => {
    const dispatch: any = useDispatch();
    const navigate = useNavigate();
    useEffect(() => {
        dispatch(api.get_Archiveslist());
    }, []);
    const Article = useSelector((state: any) => state.store.Article);
    return (
        <div className={style.RIght_list_item} id="rec_k_box">
            <div className="TitleRight">
                <li>Recommended Readings</li>
            </div>
            <ul className="itemUl">
                {Article.slice(0, 6).map((item: any, index: number) => {
                    return (
                        <li
                            key={index}
                            style={{
                                animation: `example1 ${(index + 1) * 0.1}s ease-out ${
                                    (index + 2) * 0.1
                                }s backwards`,
                            }}
                            onClick={() => {
                                navigate(`/home/details/${item.id}`, { state: { index } });
                                dispatch(api.toDateils(item.id));
                            }}
                        >
                            {
                                <span className="itemLi">
                                    <span>{item.title}</span>
                                    <span className=" Right_listTime">{time(item.publishAt)}</span>
                                </span>
                            }
                        </li>
                    );
                })}
            </ul>
        </div>
    );
};
export default Archives;
