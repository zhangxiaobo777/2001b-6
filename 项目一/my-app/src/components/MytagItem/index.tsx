import './MyTagItem.css';
import style from '../../style/style.module.css';
type Props = {
    typelist: any;
    lightIndex: number;
};

const MyTagItem = (props: Props) => {
    const { typelist, lightIndex } = props;
    return (
        <div className={style.tag_box} id="MyTagItem">
            <p>
                <span> {typelist[lightIndex].label} </span>categoryArticle
            </p>
            <p>
                totalSearch<span> {typelist[lightIndex].articleCount} </span>piece
            </p>
        </div>
    );
};

export default MyTagItem;
