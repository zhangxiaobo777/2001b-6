import { Modal, message } from 'antd';
import { QRCodeSVG } from 'qrcode.react';
import { useRef } from 'react';
import useCanvas from '../../hook/useCanvas';
type Props = {
    modalObj: any;
    handleCancel: () => void;
    openss: boolean;
};
const Shore = (props: Props) => {
    const { modalObj, handleCancel, openss } = props;
    const canvasDom = useRef(null);
    const canvasUrl = useCanvas(modalObj, canvasDom);
    const success = () => {
        message.success('海报生成成功');
    };
    return (
        <div>
            <Modal
                title="shareNamespace.title"
                okText={
                    <a href={canvasUrl} download="canvas.png">
                        下载
                    </a>
                }
                cancelText="关闭"
                okType="danger"
                //弹框是否显示
                open={openss}
                //确定
                onOk={success}
                //取消
                onCancel={handleCancel}
            >
                <div className="modal">
                    <canvas ref={canvasDom} style={{ display: 'none' }}>
                        你的浏览器不支持canvas
                    </canvas>

                    {modalObj.cover ? <img src={modalObj.cover} alt="" /> : ''}
                    <h2>{modalObj.title}</h2>
                    <p>{modalObj.summary}</p>
                    <QRCodeSVG value={modalObj.cover} />
                </div>
            </Modal>
        </div>
    );
};

export default Shore;
