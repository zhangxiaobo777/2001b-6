import { Modal, Input, Button } from 'antd';
import './login.css';
import { GithubOutlined } from '@ant-design/icons';

type Props = {
    isLoginModalOpen: boolean;
    MyhandleOk: () => void;
    MyhandleCancel: () => void;
};

const Login = (props: Props) => {
    const { isLoginModalOpen, MyhandleOk, MyhandleCancel } = props;
    return (
        <div className="login">
            <Modal
                footer
                title="Please set your information"
                width={370}
                open={isLoginModalOpen}
                onOk={MyhandleOk}
                onCancel={MyhandleCancel}
            >
                <div className="login_content">
                    <Input placeholder="Username" className="Loginp" />
                    <Input placeholder="Password" className="Loginp" />
                    <Button type="primary" danger block onClick={() => MyhandleCancel()}>
                        Login
                    </Button>
                    <div className="login_iconBox">
                        <GithubOutlined className="login_icon" />
                    </div>
                </div>
            </Modal>
        </div>
    );
};

export default Login;
