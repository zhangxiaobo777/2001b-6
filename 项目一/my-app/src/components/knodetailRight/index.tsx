/* eslint-disable array-callback-return */
import React, { useEffect } from 'react';
import './index.css';
import { EyeOutlined, ShareAltOutlined } from '@ant-design/icons';
import { useNavigate, useParams } from 'react-router-dom';
import * as api from '../../api';
import { useDispatch } from 'react-redux';
import './index';
import { Empty } from 'antd';
import style from '../../style/style.module.css';
import { time } from '../../utils/time';
type Props = {
    otherKnowledges: any;
    setIsModalOpen: any;
};
const Index = (props: Props) => {
    const { otherKnowledges, setIsModalOpen } = props;
    const navigate = useNavigate();
    const dispatch: any = useDispatch();
    const params = useParams();

    useEffect(() => {
        dispatch(api.getknowDetail(params.id as string));
        dispatch(api.otherKnowledges(params.id as string));
    }, [dispatch]);
    return (
        <ul className="right" id={style.uul}>
            <div className="list">
                {otherKnowledges.length ? (
                    otherKnowledges.map((item: any, index: number) => {
                        if (item.status === 'publish') {
                            return (
                                <li
                                    key={index}
                                    onClick={() => {
                                        navigate(`/home/knowdetail`);
                                        setTimeout(() => {
                                            navigate(`/home/knowdetail/${item.id}`);
                                        }, 100);
                                    }}
                                >
                                    <p>
                                        <h3>{item.title}</h3>
                                        <span>{time(item.createAt)}</span>
                                    </p>

                                    <dl>
                                        <dt>
                                            <img src={item.cover} alt="" />
                                        </dt>
                                        <dd>
                                            <h4>{item.summary}</h4>
                                            <p>
                                                <span>
                                                    <EyeOutlined />
                                                </span>
                                                <span>{item.views}</span>
                                                <span
                                                    onClick={(e) => {
                                                        e.stopPropagation();
                                                    }}
                                                >
                                                    <ShareAltOutlined
                                                        onClick={() => {
                                                            dispatch(
                                                                api.getknowDetailModal(item.id)
                                                            );
                                                            setIsModalOpen(true);
                                                        }}
                                                    />
                                                </span>
                                            </p>
                                        </dd>
                                    </dl>
                                </li>
                            );
                        }
                    })
                ) : (
                    <Empty />
                )}
            </div>
        </ul>
    );
};

export default Index;
