import { useState } from 'react';
import { SmileOutlined, UserOutlined } from '@ant-design/icons';
import { Input, Button } from 'antd';
import style from '../../style/style.module.css';
import Login from '../login';
import './inputBox.css';
type Props = {
    isRelease?: boolean;
    Stow?: () => void;
};

const InputBox = (props: Props) => {
    const { TextArea } = Input;
    const [isLoginModalOpen, setIsLoginModalOpen] = useState(false);
    const { isRelease, Stow } = props;
    return (
        <div className={style.myitems}>
            <div className="comment_content">
                <span>
                    <UserOutlined />
                </span>
                <TextArea
                    rows={6}
                    placeholder="commentNamespace.replyPlaceholder"
                    maxLength={200}
                    onClick={() => {
                        setIsLoginModalOpen(true);
                    }}
                />
            </div>
            <div className="comment_footer">
                <span onClick={() => setIsLoginModalOpen(true)}>
                    {/* <Popover placement="topLeft" content={"content"} trigger="click"><SmileOutlined /> <span>Emoji</span></Popover> */}
                    <SmileOutlined /> <span> Emoji</span>
                </span>
                <div>
                    {isRelease && <Button onClick={Stow}>收起</Button>}
                    &emsp;
                    <Button disabled>发布</Button>
                </div>
            </div>
            <Login
                isLoginModalOpen={isLoginModalOpen}
                MyhandleOk={() => setIsLoginModalOpen(true)}
                MyhandleCancel={() => setIsLoginModalOpen(false)}
            ></Login>
        </div>
    );
};

export default InputBox;
