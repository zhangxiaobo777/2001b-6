import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { get_tag_list } from '../../api/api';
import { Mydispatch } from '../../utils/type';
import './tag.css';
import style from '../../style/style.module.css';
type Props = {};

const MyTag = (props: Props) => {
    const navigate = useNavigate();
    const dispatch: Mydispatch = useDispatch();
    useEffect(() => {
        dispatch(get_tag_list());
    }, [dispatch]);
    const { taglist } = useSelector(({ store }: any) => {
        return { ...store };
    });

    return (
        <div className={style.tag_box} id="tag_box">
            <li>Tags</li>
            <div className="span_boxone">
                {taglist.length
                    ? taglist.map((item: any, index: number) => {
                          return (
                              <span
                                  key={index}
                                  onClick={() => {
                                      navigate(`/home/tag/${item.id}`, { state: { index } });
                                  }}
                              >
                                  {item.label}({item.articleCount})
                              </span>
                          );
                      })
                    : '暂无数据'}
            </div>
        </div>
    );
};

export default MyTag;
