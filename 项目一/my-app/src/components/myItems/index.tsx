import { useDispatch, useSelector } from 'react-redux';
import { useState } from 'react';
import { HeartOutlined, EyeOutlined, ShareAltOutlined } from '@ant-design/icons';
import './index.css';
import * as api from '../../api';
import Shore from '../shore';
import { Mydispatch } from '../../utils/type';
import style from '../../style/style.module.css';
import { Empty } from 'antd';
//时间
import { time } from '../../utils/time';
import { useNavigate } from 'react-router-dom';
type Props = {
    Article: any;
    label?: string;
};
const MyItems = ({ Article, label = '' }: Props) => {
    const dispatch: Mydispatch = useDispatch();
    const navigate = useNavigate();
    const [isModalOpen, setIsModalOpen] = useState(false);
    //点击弹窗获取的数据
    const modalObj = useSelector((state: any) => state.store.modalObj);
    return (
        <div className={style.myitems} id="my_item_box">
            <ul>
                {Article.length ? (
                    Article.map((item: any, index: number) => {
                        return (
                            <li
                                key={index}
                                onClick={() => {
                                    document.documentElement.scrollTop = 0;
                                    navigate(`/home/details/${item.id}`, { state: { index } });
                                    dispatch(api.toDateils(item.id));
                                }}
                            >
                                <div className="nav_title">
                                    <b>{item.title}</b>
                                    <span>{time(item.publishAt)}</span>
                                </div>
                                <dl>
                                    {item.cover ? (
                                        <dt>
                                            <img src={item.cover} alt="" />
                                        </dt>
                                    ) : (
                                        ''
                                    )}
                                    <dd>
                                        <div className="txt_box">
                                            <span>{item.summary}</span>
                                        </div>
                                        <div
                                            className="banban"
                                            onClick={(e) => {
                                                e.stopPropagation();
                                            }}
                                        >
                                            <span>
                                                <HeartOutlined /> {item.likes}
                                            </span>
                                            <span>
                                                <EyeOutlined /> {item.views}
                                            </span>
                                            <span
                                                onClick={() => {
                                                    dispatch(api.Modal_id(item.id));
                                                    setIsModalOpen(true);
                                                }}
                                            >
                                                <ShareAltOutlined /> shore{' '}
                                            </span>
                                        </div>
                                    </dd>
                                </dl>
                            </li>
                        );
                    })
                ) : (
                    <Empty>暂无数据</Empty>
                )}
            </ul>
            <Shore
                modalObj={modalObj}
                handleCancel={() => {
                    setIsModalOpen(false);
                }}
                openss={isModalOpen}
            ></Shore>
        </div>
    );
};
export default MyItems;
