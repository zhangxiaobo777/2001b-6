import { BackTop } from 'antd';
type Props = {};

const Backtop = (props: Props) => {
    return (
        <>
            <BackTop />
        </>
    );
};

export default Backtop;
