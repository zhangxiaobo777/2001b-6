import { Modal, message } from 'antd';
import { QRCodeSVG } from 'qrcode.react';
import './index.css';
type Props = {
    konwBookMoadle?: any;
    handleCancel: () => void;
    openss: boolean;
};
const Shore = (props: Props) => {
    const { konwBookMoadle, handleCancel, openss } = props;
    const error = () => {
        message.error('未知错误！！！');
    };
    return (
        <div>
            <Modal
                title="shareNamespace.title"
                okText="下载"
                cancelText="关闭"
                okType="danger"
                //弹框是否显示
                open={openss}
                //确定
                onOk={error}
                //取消
                onCancel={handleCancel}
            >
                <div className="modal">
                    {konwBookMoadle.cover ? <img src={konwBookMoadle.cover} alt="" /> : ''}
                    <span className="spans">{konwBookMoadle.title}</span>
                    <p>{konwBookMoadle.summary}</p>
                    <QRCodeSVG value={konwBookMoadle.cover} />
                </div>
            </Modal>
        </div>
    );
};

export default Shore;
