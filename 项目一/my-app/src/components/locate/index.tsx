import React, { useEffect, useState } from 'react';
import { HeartFilled, PushpinFilled, MessageFilled } from '@ant-design/icons';
import './index.css';
import Shore from '../../components/shore';
import { useDispatch, useSelector } from 'react-redux';
import { Mydispatch } from '../../utils/type';
import { change_likes } from '../../api/api';
type Props = {
    id: string;
};

const Locate = (props: Props) => {
    const dispatch: Mydispatch = useDispatch();
    const { id } = props;
    const [flag, setFlag] = useState(false);
    const [IsModalOpen, setIsModalOpen] = useState(false);
    useEffect(() => {
        dispatch(change_likes(id, flag));
    }, [flag]);
    const dateilsObj = useSelector((state: any) => state.store.ArticledateilsObj);

    return (
        <div className="position">
            <div>
                <HeartFilled
                    style={
                        flag
                            ? { fontSize: '17px', color: 'red' }
                            : { fontSize: '17px', color: '#bfbfbf' }
                    }
                    onClick={() => {
                        setFlag(!flag);
                    }}
                />
            </div>
            <div>
                <MessageFilled
                    style={{ fontSize: '17px', color: '#bfbfbf' }}
                    onClick={() => {
                        document.querySelector('.comment')?.scrollIntoView({ behavior: 'smooth' });
                    }}
                />
            </div>
            <div>
                <PushpinFilled
                    style={{ fontSize: '17px', color: '#bfbfbf', marginTop: '14px' }}
                    onClick={() => {
                        setIsModalOpen(true);
                    }}
                />
                <Shore
                    modalObj={dateilsObj}
                    handleCancel={() => {
                        setIsModalOpen(false);
                    }}
                    openss={IsModalOpen}
                ></Shore>
            </div>
        </div>
    );
};

export default Locate;
