import React, { useEffect } from 'react';
import './Archives.css';
import { useSelector, useDispatch } from 'react-redux';
import * as api from '../../api/api';

import style from '../../style/style.module.css';
import { useNavigate } from 'react-router-dom';
type Props = {};

const Archives = (props: Props) => {
    const dispatch: any = useDispatch();
    const navigate = useNavigate();
    useEffect(() => {
        dispatch(api.get_Archiveslist());
        dispatch(api.get_Articlet());
    }, [dispatch]);
    const Archiveslist = useSelector((state: any) => state.store.Archiveslist);
    const Article = useSelector((state: any) => state.store.Article);
    return (
        <div className="Archives" id={style.Archives}>
            <div className="Archives_top">
                <h3> Archives</h3>
                <p>
                    Total <span>{Article.length}</span> Count
                </p>
            </div>
            <div className="Archives_content">
                <h2 className={style.tag_box}>{Object.keys(Archiveslist)}</h2>
                <div className="Archives_content_item">
                    {Object.values(Archiveslist).map((item: any, index: number) => {
                        return (
                            <div key={index}>
                                <h3 className={style.tag_box}>{Object.keys(item)}</h3>
                                <ul>
                                    {Object.values(item).map((v: any, i: number) => {
                                        return v.map((a: any, b: number) => {
                                            return (
                                                <li
                                                    key={b}
                                                    onClick={() => {
                                                        navigate(`/home/details/${a.id}`);
                                                        dispatch(api.toDateils(a.id));
                                                    }}
                                                >
                                                    <span className="Archives_content_item_time">
                                                        {a.publishAt.substr(5, 5)}
                                                    </span>
                                                    <span className="Archives_content_item_title">
                                                        {a.title}
                                                    </span>
                                                </li>
                                            );
                                        });
                                    })}
                                </ul>
                            </div>
                        );
                    })}
                </div>
            </div>
        </div>
    );
};
export default Archives;
