/* eslint-disable no-empty-pattern */
/* eslint-disable react-hooks/rules-of-hooks */
import React, { useState, useEffect } from 'react';
const isLight = () => {
    const currentDate = new Date();
    return currentDate.getHours() > 18 && currentDate.getHours() < 6;
};
type Props = {};

function Index({}: Props) {
    const [theme, setTheme] = useState(() => (isLight() ? '🌙' : '☀'));
    useEffect(() => {
        //更改根结点的类名
        document.documentElement.className = theme === '🌙' ? 'light' : 'dark';
    }, [theme]);
    return (
        <div>
            <li
                onClick={() => {
                    setTheme(theme === '🌙' ? '☀' : '🌙');
                }}
            >
                {theme}
            </li>
        </div>
    );
}

export default Index;
