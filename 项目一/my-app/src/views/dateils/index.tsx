/* eslint-disable array-callback-return */
import { useEffect, useState, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import './css/dateils.css';
import './css/markdown.scss';
import './css/var.scss';
import { Affix, Image, Pagination } from 'antd';
import Right_item from '../../components/right_list';
import style from '../../style/style.module.css';
import Locate from '../../components/locate';
import Backtop from '../../components/backtop';
import { Modal } from 'antd';
import Cmments from '../../components/cmments';
import CommentInputBox from '../../components/commentInputBox';
import * as api from '../../api/api';
import { Mydispatch } from '../../utils/type';
import { useWinSize } from '../../hoos/index';
import { date } from '../../utils/time';
import { HeartFilled, PushpinFilled, MessageFilled } from '@ant-design/icons';
import MyItem from '../../components/myItems';
import { change_likes } from '../../api/api';
const Dateils = () => {
    const params = useParams();
    const location: any = useLocation();
    const dispatch: Mydispatch = useDispatch();
    const [pageSize, setPageSize] = useState(1);
    const [limit] = useState(5);
    const konwledge: any = useRef(null);
    const scrollH: any = useRef(null);
    const tagList = ['h1', 'h2', 'h3'];
    const navigate = useNavigate();
    const [ind, setind] = useState(0);
    const scrollHandle = () => {
        const scrollY = Math.ceil(document.documentElement.scrollTop);
        scrollH.current.map((item: any, index: any) => {
            if (scrollY >= item && scrollY < scrollH.current[index + 1]) {
                setind(index);
            }
        });
    };
    const changeIndex = (ind: any) => {
        setind(ind);
        document.documentElement.scrollTop = scrollH.current[ind];
    };
    const [isModalOpen, setIsModalOpen] = useState(false);
    useEffect(() => {
        dispatch(api.get_comment(params.id as string));
        // 获取标签
        const TNodeds = [...konwledge.current.children].filter((item: any) => {
            return tagList.includes(item.nodeName.toLowerCase());
        });
        // 获取最后元素
        const lastN = TNodeds[TNodeds.length - 1];
        scrollH.current = TNodeds.map((item: any) => item.offsetTop);
        lastN && scrollH.current.push(lastN.offsetTop + lastN.offsetHeight);
        window.addEventListener('scroll', scrollHandle, false);
        return () => {
            window.removeEventListener('scroll', scrollHandle, false);
        };
    }, []);
    const commentList = useSelector((state: any) => state.store.commentList);
    const paginList = commentList.slice((pageSize - 1) * limit, pageSize * limit);
    const size = useWinSize();
    const dateilsObj = useSelector((state: any) => state.store.ArticledateilsObj);
    const Article = useSelector((state: any) => state.store.Article);
    let newArticle = Article.filter((item: any) => {
        return item.id !== params.id;
    });
    console.log(newArticle);
    const [flag] = useState(false);

    useEffect(() => {
        dispatch(change_likes(params.id as string, flag));
    }, [flag]);
    useEffect(() => {
        location.state.index === 0 && setIsModalOpen(true);
    }, []);
    const handleOk = () => {
        setIsModalOpen(false);
        window.location.href =
            'https://openapi.alipaydev.com/gateway.do?method=alipay.trade.page.pay&app_id=2021000121670807&charset=utf-8&version=1.0&sign_type=RSA2&timestamp=2022-09-25%2020%3A43%3A22&notify_url=https%3A%2F%2Fwww.baidu.com&return_url=http%3A%2F%2Flocalhost%3A3000%2Fhome%2Farticle&sign=M7vTaf48hl1wIZIvxtTVj7mh1byQOxUnQR2fEMTXH%2Bnmrbfsgra3qYB0cNDlRKPQTKMY1oPHL4RXKyYe63ywSdLQDWKOkKXBTkq%2B8L5OFMRT3szDl2WOH5Ec3ac8YhehhLrVFviBy%2BSBQAOsVH9jjrPzewPxDIZo%2FHndTK3LVXK46OgOiVlhOO7PTXtleVZ%2Br0%2FmX2eZMjAlQC%2FPWeGZg5qI4jhJUJiKjpUJ2R1yVJWXHJshs%2FPrd4kOOvDIE4chk7gvcdEmCWpI81%2BxcXb5Xfmc%2BfG5xZVRjOlyYsBVVvoehIHhySG5ICaGXPvYOYW0dqO%2FQ0sb0zw3MR2%2Bs8YGoQ%3D%3D&alipay_sdk=alipay-sdk-nodejs-3.2.0&biz_content=%7B%22out_trade_no%22%3A%2215693802373221aaa%22%2C%22product_code%22%3A%22FAST_INSTANT_TRADE_PAY%22%2C%22total_amount%22%3A%221000%22%2C%22subject%22%3A%22%E5%95%86%E5%93%81%22%2C%22body%22%3A%22%E5%95%86%E5%93%81%E8%AF%A6%E6%83%85%22%7D';
    };
    const handleCancel = () => {
        navigate(-1);
    };
    return (
        <div className="con_box">
            {isModalOpen && (
                <div className="ali_pay">
                    <Modal
                        title="前往支付宝支付"
                        open={isModalOpen}
                        onOk={handleOk}
                        onCancel={handleCancel}
                    >
                        <p>{dateilsObj.title}</p>
                        <p>1000元</p>
                    </Modal>
                </div>
            )}
            <div className="con_left_box">
                {/* 图片标题  con_left_box*/}
                <div className={style.myitems} id="dateils_top">
                    {size.width >= 970 ? (
                        <Locate id={dateilsObj.id}></Locate>
                    ) : (
                        <div className="footer_btn">
                            <div className="btn">
                                <HeartFilled></HeartFilled>
                            </div>
                            <div className="btn">
                                <MessageFilled
                                    style={{ fontSize: '17px', color: '#bfbfbf' }}
                                    onClick={() => {
                                        document
                                            .querySelector('.comment')
                                            ?.scrollIntoView({ behavior: 'smooth' });
                                    }}
                                ></MessageFilled>
                            </div>
                            <div className="btn">
                                <PushpinFilled style={{ fontSize: '17px', color: '#bfbfbf' }} />
                            </div>
                        </div>
                    )}
                    {dateilsObj.cover && <Image width="100%" src={dateilsObj.cover} />}
                    <div className="dateils_top_content">
                        <h1>{dateilsObj.title}</h1>
                        <p>
                            <span>publishAt {date(dateilsObj.publishAt)}</span> •{' '}
                            <span>readings{dateilsObj.views}</span>
                        </p>
                    </div>
                </div>
                {/* markdown渲染 */}
                <div
                    ref={konwledge}
                    className="markdown"
                    id={style.markdown}
                    dangerouslySetInnerHTML={{ __html: dateilsObj.html }}
                ></div>
                {/* 版权信息 */}
                <div className={style.myitems} id="copyright">
                    <p>
                        发布于<span>{dateilsObj.publishAt}</span> | 版权信息：非商用-署名-自由转载
                    </p>
                </div>
                {/* 评论 */}
                <div className="comment">
                    <p>comment</p>
                    {/* 评论输入框 */}
                    {dateilsObj.isCommentable ? (
                        <div>
                            <CommentInputBox isRelease={false}></CommentInputBox>
                        </div>
                    ) : (
                        <></>
                    )}
                    {/* 评论内容 */}

                    <Cmments list={paginList}></Cmments>

                    {/* 评论分页 */}
                    <div className="pagination">
                        {commentList.length ? (
                            <Pagination
                                onChange={(page) => {
                                    setPageSize(page);
                                }}
                                defaultCurrent={1}
                                total={commentList.length}
                                pageSize={limit}
                                current={pageSize}
                            />
                        ) : (
                            <></>
                        )}
                    </div>
                </div>
                {/* 推荐阅读 */}
                <div className="details_recommend">
                    <p>
                        {/* Recommended Readings */}
                        推荐阅读
                    </p>
                    <div className="details_recommend_cntent">
                        <MyItem Article={newArticle}></MyItem>
                    </div>
                </div>
            </div>
            {size.width >= 970 ? (
                <div className="con_right_box">
                    <Affix onChange={(affixed) => console.log(affixed)}>
                        <div>
                            <div className={style.recommendToReading_box}>
                                <Right_item></Right_item>
                            </div>
                            <div className="tips_box">
                                <h2>目录</h2>
                                {JSON.parse(dateilsObj.toc) &&
                                    JSON.parse(dateilsObj.toc).map((item: any, index: number) => {
                                        return (
                                            <p
                                                onClick={() => changeIndex(index)}
                                                key={index}
                                                className={index === ind ? 'active' : ''}
                                            >
                                                {item.text}
                                            </p>
                                        );
                                    })}
                            </div>
                        </div>
                    </Affix>
                    <Backtop></Backtop>
                </div>
            ) : (
                ''
            )}
        </div>
    );
};
export default Dateils;
