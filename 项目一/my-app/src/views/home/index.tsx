/* eslint-disable react-hooks/rules-of-hooks */

import './home.css';
import { Outlet, NavLink } from 'react-router-dom';
import style from '../../style/style.module.css';
import { TranslationOutlined, GithubOutlined, WifiOutlined } from '@ant-design/icons';
import { useState } from 'react';
import i18n from '../../i18n';
import { useTranslation } from 'react-i18next';
import { SearchOutlined, UnorderedListOutlined, CloseOutlined } from '@ant-design/icons';
import './home.css';
import Switch from '../../components/style_Switch';
import { useNavigate } from 'react-router-dom';
import { useWinSize } from '../../hoos/index';
import type { DrawerProps } from 'antd';
import { Drawer } from 'antd';
// const changeLight=()=>{
//     let Currentdate=new Date()
//     return Currentdate.getHours()>=6&&Currentdate.getHours()<=19
// }
const index = () => {
    const [open, setOpen] = useState(false);
    // eslint-disable-next-line no-unused-vars, @typescript-eslint/no-unused-vars
    const [placement, setPlacement] = useState<DrawerProps['placement']>('top');

    const showDrawer = () => {
        setOpen(true);
    };
    const onClose = () => {
        setOpen(false);
    };
    const size = useWinSize();
    const searchgate = useNavigate();
    const [lang, setLang] = useState(() => navigator.language);
    const { t } = useTranslation();
    const items = [
        {
            id: 1,
            title: 'article',
            path: '/home/article',
        },
        {
            id: 2,
            title: 'archive',
            path: '/home/archive',
        },
        {
            id: 3,
            title: 'knowledgeBooks',
            path: '/home/knowlage',
        },
    ];
    window.onscroll = () => {
        if (document.documentElement.scrollTop >= 64) {
            let box = document.querySelector('.ant-drawer-content-wrapper');
            box?.classList.add('scrollTo_mask');
        } else {
            document.querySelector('.scrollTo_mask')?.classList.remove('scrollTo_mask');
            document.querySelector('.scrollTo_mask')?.classList.add('scrollTo_mask');
        }
    };
    return (
        <div className="index">
            <header className={style.headerBox}>
                <div className="top_nav_box">
                    <div className="tab_nav">
                        <div className="logo_box">
                            <img
                                src="https://bwcreation.oss-cn-beijing.aliyuncs.com/2022-09-05/001.gif"
                                alt=""
                                className="photo"
                            />
                        </div>
                        {size.width >= 960 ? (
                            <div className="txt_box">
                                <div className="tab_title_txt">
                                    {items.map((item, index) => {
                                        return (
                                            <NavLink
                                                className={style.tag_box}
                                                to={item.path}
                                                key={index}
                                            >
                                                {t(item.title)}
                                            </NavLink>
                                        );
                                    })}
                                </div>
                                <div className="tab_title_icon">
                                    <TranslationOutlined
                                        onClick={() => {
                                            const newLang = lang === 'en' ? 'zh-CN' : 'en';
                                            setLang(newLang);
                                            i18n.changeLanguage(newLang);
                                        }}
                                    />
                                    <Switch></Switch>
                                    <SearchOutlined
                                        onClick={() => {
                                            searchgate('/search');
                                        }}
                                    />
                                </div>
                            </div>
                        ) : (
                            <div className="txt_box">
                                <div className="txt_icon">
                                    {open ? (
                                        <CloseOutlined onClick={onClose} />
                                    ) : (
                                        <UnorderedListOutlined onClick={showDrawer} />
                                    )}
                                </div>
                                <div className="txt_mask">
                                    <Drawer
                                        placement={placement}
                                        closable={false}
                                        onClose={onClose}
                                        open={open}
                                        key={placement}
                                        closeIcon
                                        mask={false}
                                    >
                                        <ul id="content_mask" className={style.tag_box}>
                                            {items.map((item, index) => {
                                                return (
                                                    <li key={index} onClick={onClose}>
                                                        <NavLink
                                                            className={style.tag_box}
                                                            to={item.path}
                                                            key={index}
                                                        >
                                                            {t(item.title)}
                                                        </NavLink>
                                                    </li>
                                                );
                                            })}
                                            <li>
                                                {' '}
                                                <SearchOutlined
                                                    onClick={() => {
                                                        searchgate('/search');
                                                    }}
                                                />
                                            </li>
                                            <li>
                                                {' '}
                                                <Switch></Switch>
                                            </li>
                                            <li>
                                                {' '}
                                                <TranslationOutlined
                                                    onClick={() => {
                                                        const newLang =
                                                            lang === 'en' ? 'zh-CN' : 'en';
                                                        setLang(newLang);
                                                        i18n.changeLanguage(newLang);
                                                    }}
                                                />
                                            </li>
                                        </ul>
                                    </Drawer>
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            </header>
            <main className={style.HomeMain}>
                <div className="container">
                    <Outlet></Outlet>
                </div>
            </main>
            <footer>
                <div className="footer_icon">
                    <div className="icons">
                        <a href="https://creation.shbwyz.com/rss" target="view_window">
                            <WifiOutlined rotate={45} />
                        </a>
                    </div>
                    <div className="icons">
                        <a href="https://github.com/fantasticit/wipi" target="view_window">
                            <GithubOutlined></GithubOutlined>
                        </a>
                    </div>
                </div>
            </footer>
        </div>
    );
};

export default index;
