/* eslint-disable react/jsx-pascal-case */
import MyItems from '../../components/myItems';
import Myswiper from '../../components/myswiper';
import MyTag from '../../components/tag';
import style from '../../style/style.module.css';
import './categoryTitle.css';
import { Mydispatch } from '../../utils/type';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useState } from 'react';
import { get_type_list } from '../../api/api';
import MytagItem from '../../components/mytagItem';
import Right_item from '../../components/right_list';
import { Affix } from 'antd';
import { useWinSize } from '../../hoos';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
type Props = {};

const CategoryTitle = (props: Props) => {
    const dispatch: Mydispatch = useDispatch();
    const size = useWinSize();
    const navigate = useNavigate();
    const location: any = useLocation();
    const [lightIndex, setLightIndex] = useState(() => location.state && location.state.index);
    const [swiperFlag, setSwiperFlag] = useState(false);
    const params = useParams();
    useEffect(() => {
        dispatch(get_type_list());
    }, [dispatch]);
    const { typelist, Article } = useSelector(({ store }: any) => {
        return { ...store };
    });
    console.log(Article, params.id, '123');
    const ArticleOnce = Article.filter(
        (item: any, index: number) => item.category && item.category.label === params.id
    );
    return (
        <div className="con_box">
            <div className="con_left_box">
                <div className="swiper_box" id={style.myswiper}>
                    {swiperFlag ? (
                        <Myswiper></Myswiper>
                    ) : (
                        <MytagItem typelist={typelist} lightIndex={lightIndex}></MytagItem>
                    )}
                </div>

                <div className={style.article_box} id="article_box">
                    <ul id="type_box">
                        {typelist.length > 0 ? (
                            <li
                                className={lightIndex === -1 ? 'type_light' : ''}
                                onClick={() => {
                                    navigate('/home/article');
                                }}
                            >
                                all
                            </li>
                        ) : (
                            ''
                        )}
                        {typelist.map((item: any, index: number) => {
                            return (
                                item.id && (
                                    <li
                                        key={index}
                                        className={lightIndex === index ? 'type_light' : ''}
                                        onClick={() => {
                                            setLightIndex(index);
                                            setSwiperFlag(false);
                                            navigate(`/home/categoryTitle/${item.label}`, {
                                                state: {
                                                    index,
                                                },
                                            });
                                        }}
                                    >
                                        {item.label}
                                    </li>
                                )
                            );
                        })}
                    </ul>
                    <MyItems Article={ArticleOnce} label={params.id}></MyItems>
                </div>
            </div>
            {size.width >= 970 ? (
                <div className="con_right_box">
                    <Affix offsetTop={0} onChange={(affixed) => console.log(affixed)}>
                        <div className="recommendToReading_box">
                            <Right_item></Right_item> <MyTag></MyTag>
                        </div>
                    </Affix>
                </div>
            ) : (
                ''
            )}
        </div>
    );
};
export default CategoryTitle;
