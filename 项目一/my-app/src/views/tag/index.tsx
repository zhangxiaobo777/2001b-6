/* eslint-disable react/jsx-pascal-case */
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './tag.css';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { Mydispatch } from '../../utils/type';
import { get_tag_list } from '../../api/api';
import { Empty } from 'antd';
import { time } from '../../utils/time';
import style from '../../style/style.module.css';
import { HeartOutlined, EyeOutlined, ShareAltOutlined } from '@ant-design/icons';
import Right_list from '../../components/right_list';
import Category from '../../components/category';
type Props = {};

const Tag = (props: Props) => {
    const dispatch: Mydispatch = useDispatch();
    const location: any = useLocation();
    useEffect(() => {
        dispatch(get_tag_list());
    }, [dispatch]);
    const params = useParams();
    const navigate = useNavigate();
    const { taglist, Article } = useSelector(({ store }: any) => {
        return { ...store };
    });
    const [lightNum, setLightNum] = useState(() => (location.state ? location.state.index : 0));
    const currentData = taglist.filter((item: any) => item.id === params.id)[0];
    const currentCon = Article.filter(
        (item: any) => item.tags.length > 0 && item.tags[0].id === params.id
    );
    return (
        <div className="con_box">
            <div className="con_left_box">
                <div id="tagRelativeArticles_box" className={style.tag_box}>
                    <p>
                        yu<span>{currentData.label}</span>tagRelativeArticles
                    </p>
                    <p>
                        totalSearch<span>{currentData.articleCount}</span>piece
                    </p>
                </div>
                <div id="tagRelativeArticles_nav" className={style.tag_box}>
                    <p className="nav_title">
                        <h3 className={style.tag_box}>tagTitle</h3>
                    </p>
                    <p id="nav_con" className={style.tag_box}>
                        {taglist.length > 0
                            ? taglist.map((item: any, index: number) => {
                                  return (
                                      <span
                                          key={index}
                                          className={lightNum === index ? 'on' : ''}
                                          onClick={() => {
                                              setLightNum(index);
                                              navigate(`/home/tag/${item.id}`);
                                          }}
                                      >
                                          {item.label}({item.articleCount})
                                      </span>
                                  );
                              })
                            : ''}
                    </p>
                </div>
                <div id="tagRelativeArticles_con" className={style.tag_box}>
                    {currentCon.length > 0 ? (
                        currentCon.map((item: any, index: number) => {
                            return (
                                <div id="tagRelativeArticles_con_item" key={index}>
                                    <p>
                                        <b>{item.title}</b> | <span>{time(item.createAt)}</span>
                                    </p>
                                    <dl>
                                        <dt>
                                            <img src={item.cover} alt="" />
                                        </dt>
                                        <dd>
                                            <p>{item.summary}</p>
                                            <p>
                                                <span>
                                                    <HeartOutlined /> {item.likes}
                                                </span>
                                                <span>
                                                    <EyeOutlined /> {item.views}
                                                </span>
                                                <span>
                                                    <ShareAltOutlined />
                                                    share
                                                </span>
                                            </p>
                                        </dd>
                                    </dl>
                                </div>
                            );
                        })
                    ) : (
                        <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
                    )}
                </div>
            </div>
            <div className="con_right_box">
                <Right_list></Right_list>
                <Category></Category>
            </div>
        </div>
    );
};

export default Tag;
