/* eslint-disable react/jsx-pascal-case */
import React from 'react';
import Know_left from '../../components/know_left';
import Right_item from '../../components/right_list';
import Category from '../../components/category';
import { Affix } from 'antd';
import { useWinSize } from '../../hoos/index';
import style from '../../style/style.module.css';
type Props = {};

const Knowlage = (props: Props) => {
    const size = useWinSize();
    return (
        <div className="con_box">
            <div className="con_left_box">
                <div className={style.tag_box}>
                    <Know_left></Know_left>
                </div>
            </div>
            {size.width >= 970 ? (
                <div className="con_right_box">
                    <Affix>
                        <div className="recommendToReading_box">
                            <Right_item></Right_item>
                            <Category></Category>
                        </div>
                    </Affix>
                </div>
            ) : (
                ''
            )}
        </div>
    );
};

export default Knowlage;
