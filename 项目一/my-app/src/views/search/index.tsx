import './index.css';
import { CloseOutlined } from '@ant-design/icons';
import { useNavigate } from 'react-router-dom';
import { Input } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { Mydispatch } from '../../utils/type';
import * as api from '../../api';

type Props = {};
const Searchs = (props: Props) => {
    const navigate = useNavigate(); //点击esc和X返回首页

    const dispatch: Mydispatch = useDispatch();
    const onSearch = (value: string) => {
        dispatch(api.get_search(value));
    };
    const { Search } = Input;
    //搜索数据
    const newSearch = useSelector((state: any) => state.store.newSearch);
    // console.log(newSearch, 'hsp');

    return (
        <div className="serach">
            <div className="box">
                <div className="header">
                    <div className="box_1">
                        <h2>searchArticle</h2>
                    </div>
                    <div
                        className="box_2"
                        onClick={() => {
                            navigate('/home/article');
                        }}
                    >
                        <span>
                            <CloseOutlined />
                        </span>
                        <span>esc</span>
                    </div>
                </div>
                <div className="box_3">
                    <Search
                        placeholder="searchArticlePlaceholder"
                        onSearch={onSearch}
                        enterButton
                    />
                </div>
                <div className="search_list">
                    {newSearch &&
                        newSearch.map((item: any, index: number) => {
                            return (
                                <li
                                    key={index}
                                    onClick={() => {
                                        navigate(`/home/details/${item.id}`);
                                        dispatch(api.toDateils(item.id));
                                    }}
                                >
                                    {item.title}
                                </li>
                            );
                        })}
                </div>
            </div>
        </div>
    );
};

export default Searchs;
