/* eslint-disable react/jsx-pascal-case */
import './archicve.css';
import Category from '../../components/category';
import Right_item from '../../components/right_list';
import Archives from '../../components/archives';
import style from '../../style/style.module.css';
import Backtop from '../../components/backtop';
import { Affix } from 'antd';
import { useWinSize } from '../../hoos/index';
type Props = {};

const Archicve = (props: Props) => {
    const size = useWinSize();
    return (
        <div className="con_box">
            <div className={style.con_left_box} id="con_left_box">
                <div className="article_box">
                    <Archives></Archives>
                </div>
            </div>
            {size.width >= 970 ? (
                <div className="con_right_box">
                    <Affix offsetTop={0} onChange={(affixed) => console.log(affixed)}>
                        <div className="recommendToReading_box">
                            <Right_item></Right_item> <Category></Category>
                        </div>
                    </Affix>
                </div>
            ) : (
                ''
            )}
            <Backtop></Backtop>
        </div>
    );
};
export default Archicve;
