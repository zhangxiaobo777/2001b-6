const { createProxyMiddleware } = require('http-proxy-middleware');
module.exports = (app) => {
    app.use(
        '/api',
        createProxyMiddleware({
            target: 'https://creationapi.shbwyz.com',
            changeOrigin: true,
            pathRewrite(path) {
                return path.replace('/api', '');
            },
        })
    );
    app.use(
        '/ali_pay',
        createProxyMiddleware({
            target: 'localhost:9000',
            changeOrigin: true,
            pathRewrite(path) {
                return path.replace('/ali_pay', '');
            },
        })
    );
};
