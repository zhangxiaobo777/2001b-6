import { useState, useEffect } from 'react';
type Props = {
    data: any;
    canvasDom: any;
};

const getImg = (url: Props) => {
    return new Promise((resolve, reject) => {
        //  实例化image
        const img = new Image();
        img.crossOrigin = 'anonymous'; // 允许跨域
        img.onload = () => {
            resolve(img);
        };
        img.onerror = (error) => {
            reject(error);
        };
        img.src = url + '?v=' + Math.random();
    });
};
const drawUrl = async (ctx: any, data: any, canvasDom: any) => {
    //  绘制填充背景
    ctx.fillStyle = '#fff';
    ctx.fillRect(0, 0, 400, 479);
    //  绘制标题
    ctx.font = '30px hotpink';
    ctx.fillStyle = '#000';
    ctx.fillText(data.title, 124, 30);

    //  绘制标题下面的线
    ctx.beginPath();
    //  开始绘制
    ctx.stroke();
    //  绘制图片
    const img = await getImg(data.cover);
    console.log(img);

    ctx.drawImage(img, 50, 100, 300, 200);

    //  绘制下面的文字
    ctx.font = '12px yellow';
    ctx.fillStyle = '#000';
    ctx.fillText(data.title, 50, 450);
    ctx.fillText(data.summary, 50, 470);

    //  生成url 把canvas转成图片
    const imgUrl = canvasDom.current.toDataURL();
    // console.dir(imgUrl, 'img');
    return imgUrl;
};

//  生成canvas
const createCanvas = async (data: any, canvasDom: any) => {
    //  设置canvas的宽高

    canvasDom.current.width = 400;
    canvasDom.current.height = 479;
    //  创建上下文对象
    const ctx = canvasDom.current.getContext('2d');
    //  绘制
    let imgUrl = await drawUrl(ctx, data, canvasDom);
    return imgUrl;
};

const useCanvas = (data: any, canvasDom: any) => {
    // const {}=props
    const [url, setUrl] = useState('');
    useEffect(() => {
        (async () => {
            let url = await createCanvas(data, canvasDom);
            setUrl(url);
        })();
    }, [data]);
    return url;
};
export default useCanvas;
