import i18next from 'i18next';

import { initReactI18next } from 'react-i18next'; //在react中引用i18next
import LanguageDetector from 'i18next-browser-languagedetector'; //用来检测语言
import en from './config/en.config'; //引入英文配置文档
import ch from './config/ch.config'; //引入英文配置文档
const resources = {
    en: { translation: en },
    'zh-CN': { translation: ch },
};
i18next
    .use(LanguageDetector)
    .use(initReactI18next)
    .init({
        //初始化
        resources,
        detection: {
            order: [
                'querystring',
                'cookie',
                'localStorage',
                'sessionStorage',
                'navigator',
                'htmlTag',
                'path',
                'subdomain',
            ],
        },
        interpolation: {
            escapeValue: false,
        },
    });
export default i18next;
