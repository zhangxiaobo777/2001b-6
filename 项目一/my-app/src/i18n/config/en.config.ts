/* eslint-disable import/no-anonymous-default-export */
export default {
    article: 'article',
    archive: 'archive',
    knowledgeBooks: 'knowledgeBooks',
    markdown: 'markdown',
    recommendToReading: 'recommendToReading',
};
