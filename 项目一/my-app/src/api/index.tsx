import { Dispatch } from 'redux';
import { getKnowledge, getArticle } from './interfcce';
//获取所有数据
export const get_article = () => {
    return async (dispatch: Dispatch) => {
        const data = await getArticle();
        dispatch({
            type: 'ARTICLE_LIST',
            payload: data.data.data,
        });
    };
};
//弹窗获取id
export const Modal_id = (id: string) => {
    return {
        type: 'MODAL_ID',
        payload: id,
    };
};
//获取知识小册数据
export const Konw_book_list = () => {
    return async (dispatch: Dispatch) => {
        const data = await getKnowledge();
        // console.log(data);
        dispatch({
            type: 'KONW_BOOK_LIST',
            payload: data.data.data,
        });
    };
};
//知识小册弹窗
export const konw_Modal_id = (id: string) => {
    return {
        type: 'KONW_MODAL_ID',
        payload: id,
    };
};

export const get_knowlist = () => {
    return async (dispatch: Dispatch) => {
        const data = await getKnowledge();
        console.log(data, '123');
        dispatch({
            type: 'GET_LISTget_KNOWLIST',
            payload: data.data.data[0],
        });
    };
};

export const getknowDetail = (id: string) => {
    return {
        type: 'GET_KNOWDETAIL',
        payload: id,
    };
};
export const getknowDetailModal = (id: string) => {
    // console.log(id,'888');
    return {
        type: 'GET_KNOWDETAILMODAL',
        payload: id,
    };
};

export const otherKnowledges = (id: string) => {
    return {
        type: 'GET_OTHERKNOWLEDGES',
        payload: id,
    };
};
//搜索
export const get_search = (value: string) => {
    return {
        type: 'GET_SEARCH',
        payload: value,
    };
};
// 跳详情
export const toDateils = (id: string) => {
    return {
        type: 'TODATEILS',
        payload: id,
    };
};
