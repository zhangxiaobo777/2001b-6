import request from '../../utils/Request';
//知识小册页面的接口
export const getKnowledge = () => request.get('/api/api/Knowledge');
export const getArticle = () => request.get('/api/api/article');
export const getArchives = () => request.get('/api/api/article/archives');
export const getTagList = () => request.get('/api/api/tag');
export const getCategory = () => request.get('/api/api/category');
export const getArticleRecommend = () => request.get('/api/api/article/recommend');
export const go_to_pay = () => request.get('/ali_pay/pay');
