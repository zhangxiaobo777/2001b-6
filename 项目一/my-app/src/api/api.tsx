import { Dispatch } from 'redux';
import axios from 'axios';
import {
    getArchives,
    getArticle,
    getCategory,
    getTagList,
    getArticleRecommend,
    go_to_pay,
} from '../api/interfcce';
export const get_Archiveslist = () => {
    return async (dispatch: Dispatch) => {
        const res = await getArchives();
        console.log(res);
        dispatch({
            type: 'GET_LISTget_ARCHIVESLIST',
            payload: res.data.data,
        });
    };
};
// 轮播图和总数据
export const get_Articlet = () => {
    return async (dispatch: Dispatch) => {
        const res = await getArticle();
        dispatch({
            type: 'GET_LISTget_ARTICLET',
            payload: res.data.data[0],
        });
    };
};

//tag标签
export const get_tag_list = () => {
    return async (dispatch: Dispatch) => {
        const res = await getTagList();
        dispatch({
            type: 'GET_TAG',
            payload: res.data.data,
        });
    };
};

//type类型
export const get_type_list = () => {
    return async (dispatch: Dispatch) => {
        const res = await getCategory();
        dispatch({
            type: 'GET_TYPE',
            payload: res.data.data,
        });
    };
};

//筛选首页
export const change_type_list = (index: number = -1, label: string = '') => {
    return {
        type: 'CHANGE_TYPE_LIST',
        payload: {
            index,
            label,
        },
    };
};

//列表 详情数据
export const getRightList = () => {
    return async (dispatch: Dispatch) => {
        const res = await getArticleRecommend();
        console.log(res);
        dispatch({
            type: 'GET_LISTget_recommend',
            payload: res.data.data[0],
        });
    };
};

// 跳详情
export const toDateils = (id: string) => {
    return {
        type: 'TODATEILS',
        payload: id,
    };
};

//改变likes

export const change_likes = (id: string, flag: boolean) => {
    return {
        type: 'CHANGE_LIKES',
        payload: {
            id,
            flag,
        },
    };
};
//  获取评论数据

export const get_comment = (id: string) => {
    return async (dispatch: Dispatch) => {
        let res = await axios.get(`https://creationapi.shbwyz.com/api/comment/host/${id}`);
        dispatch({
            type: 'GET_COMMENT',
            payload: res.data.data[0],
        });
    };
};

//pay
export const gotoPay = () => {
    return async (dispatch: Dispatch) => {
        let res = await go_to_pay();
        console.log(res);
    };
};
