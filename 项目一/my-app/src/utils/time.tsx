import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime'; //引入展示相对时间的插件
import updateLocale from 'dayjs/plugin/updateLocale'; //更新语言配置
import localizedFormat from 'dayjs/plugin/localizedFormat';
import 'dayjs/locale/zh-cn'; //导入本地化语言
dayjs.locale('zh-cn'); //配置语言的加载环境
dayjs.extend(updateLocale); //依赖
dayjs.extend(relativeTime); //依赖
dayjs.extend(localizedFormat);
export const time = (createTime: any) => `${dayjs(createTime).fromNow()}`; //封装时间格式化函数
export const date = (createTime: any) => `${dayjs(createTime).format('L LT')}`;
