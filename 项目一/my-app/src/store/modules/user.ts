/* eslint-disable no-duplicate-case */
import { Tp } from '../../utils/type';
const initialState: any = {
    Archiveslist: {}, //归档
    Article: [], //轮播图数据
    modalObj: {}, //主页点击分享的数据
    Konwbook: [], //知识小册页面数据
    konwBookMoadle: {}, //点击知识小册分享按钮的数据
    allAtricle: [],
    taglist: [],
    typelist: [],
    newSearch: [], //搜索
    ArticledateilsObj: JSON.parse(localStorage.getItem('ArticledateilsObj') as string) || {}, //文章跳详情
    knowlist: JSON.parse(localStorage.getItem('knowdata') as any) || [],
    knowlistinfo: {},
    otherKnowledges: [],
    knowlistinfomodal: {},
    commentList: [], //评论数据
};
const store = (state = initialState, { type, payload }: Tp) => {
    let newState = JSON.parse(JSON.stringify(state) as string);
    switch (type) {
        //搜索
        case 'GET_SEARCH':
            const searchData = state.Article.filter((item: any) => item.title.includes(payload));
            return {
                ...state,
                newSearch: searchData,
            };
        //档案
        case 'GET_LISTget_ARCHIVESLIST':
            return {
                ...state,
                Archiveslist: payload,
            };
        //总数据
        //轮播图
        case 'GET_LISTget_ARTICLET':
            return {
                ...state,
                Article: payload,
                allAtricle: payload,
            };

        //点击分享详情id
        case 'MODAL_ID':
            let obj = state.Article.filter((item: any) => item.id === payload)[0];
            // console.log(obj)
            return {
                ...state,
                modalObj: obj,
            };
        case 'GET_TAG':
            return {
                ...state,
                taglist: [...payload],
            };
        case 'GET_TYPE':
            return {
                ...state,
                typelist: [...payload],
            };
        case 'CHANGE_TYPE_LIST':
            payload.index === -1
                ? (state.Article = [...state.allAtricle])
                : (state.Article = state.allAtricle.filter(
                      (item: any) => item.category && item.category.label === payload.label
                  ));
            return {
                ...state,
                Article: [...state.Article],
            };
        //知识小册数据
        case 'KONW_BOOK_LIST':
            return {
                ...state,
                Konwbook: payload,
            };

        //点击知识小册分享详情id
        case 'KONW_MODAL_ID':
            let obj1 = state.Konwbook[0].filter((item: any) => {
                return item.id === payload;
            })[0];
            return {
                ...state,
                konwBookMoadle: obj1,
            };
        case 'GET_TAG':
            return {
                ...state,
                knowlist: payload,
            };
        case 'GET_LISTget_KNOWLIST':
            localStorage.setItem('knowdata', JSON.stringify(newState.knowlist));
            return {
                ...state,
                knowlist: payload,
            };
        case 'GET_KNOWDETAIL':
            const localList = localStorage.getItem('knowdata');
            const list = localList ? JSON.parse(localList) : [];
            list.forEach((item: any) => {
                if (item.id === payload) {
                    newState.knowlistinfo = item;
                }
            });
            return newState;
        case 'GET_KNOWDETAILMODAL':
            const localList2 = localStorage.getItem('knowdata');
            const list2 = localList2 ? JSON.parse(localList2) : newState.knowlist;
            list2.forEach((item: any) => {
                console.log(payload, 'payload');

                if (item.id === payload) {
                    newState.knowlistinfomodal = item;
                }
            });
            return newState;
        case 'GET_OTHERKNOWLEDGES':
            let localList1 = localStorage.getItem('knowdata');
            let list1 = localList1 ? JSON.parse(localList1) : state.knowlist;
            console.log(payload);
            let newList1 = list1.filter((item: any) => item.id !== payload);
            return {
                ...state,
                otherKnowledges: newList1,
            };
        //   Article
        case 'TODATEILS':
            newState.ArticledateilsObj = newState.Article.filter(
                (item: any) => item.id === payload
            )[0];
            localStorage.setItem('ArticledateilsObj', JSON.stringify(newState.ArticledateilsObj));
            return newState;

        //likes
        case 'CHANGE_LIKES':
            let index = newState.Article.findIndex((item: any) => item.id === payload.id);
            if (payload.flag) {
                (newState.Article[index].likes as number) = 11;
            } else {
                (newState.Article[index].likes as number) = 9;
            }
            return newState;
        // 评论数据
        case 'GET_COMMENT':
            newState.commentList = payload;
            return newState;
        default:
            return {
                ...state,
            };
    }
};
export default store;
