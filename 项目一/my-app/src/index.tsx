import ReactDOM from 'react-dom/client';
import './index.css';
import RouteViews from './router/RouteViews';
import { Provider } from 'react-redux';
import store from './store';
import './style/theme.css';

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement);
root.render(
    <Provider store={store}>
        <RouteViews />
    </Provider>
);
