import { useState, useCallback, useEffect } from 'react';

export const useWinSize = () => {
    const [size, setsize] = useState({
        width: document.documentElement.clientWidth,
        height: document.documentElement.clientHeight,
    });

    const changeSize = useCallback(() => {
        setsize({
            width: document.documentElement.clientWidth,
            height: document.documentElement.clientHeight,
        });
    }, []);

    useEffect(() => {
        //    绑定页面监听 组件销毁的时候
        window.addEventListener('resize', changeSize);
        //  清楚绑定
        return () => {
            window.removeEventListener('resize', changeSize);
        };
    }, []);
    return size;
};
