module.exports = {
    extends: ['react-app', 'react-app/jest', 'prettier'],
    plugins: ['prettier'],
    rules: {
        'prettier/prettier': [
            'error',
            {
                singleQuote: true,
                printWidth: 100,
                tabWidth: 4,
                trailingComma: 'es5',
                endOfLine: 'auto',
                semi: true,
            },
        ],
        'no-console': process.env.NODE_ENV === 'development' ? 'off' : 'off',
        'no-unused-vars': 'error',
        'no-ebugger': process.env.NODE_ENV === 'development' ? 'off' : 'off',
    },
};
